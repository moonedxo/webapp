import { Component, OnInit } from '@angular/core';
declare var $: any;
import { AfterViewInit,ElementRef} from '@angular/core';

@Component({
  selector: 'app-pillsliders',
  templateUrl: './pillsliders.component.html',
  styleUrls: ['./pillsliders.component.css']
})
export class PillslidersComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngAfterViewInit()
  {
    var s=document.createElement("script");
    s.type="text/javascript";
    s.innerHTML="console.log('done');"; //inline script
    s.src="./pillslider.js"; //external script
  }

  ngOnInit(): void {

  }

}
