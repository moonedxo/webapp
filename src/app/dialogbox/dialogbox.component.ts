import { Component, OnInit } from '@angular/core';
declare var $: any;
import { FormControl,FormGroup,Validators, FormBuilder} from '@angular/forms';
import { dialogboxInput } from "../shared/services/dialogboxInput";
import { DatePipe } from "@angular/common";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { Router } from "@angular/router";
import { AuthService } from "../shared/services/auth.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-dialogbox',
  templateUrl: './dialogbox.component.html',
  styleUrls: ['./dialogbox.component.css']
})
export class DialogboxComponent implements OnInit {

  myForm: FormGroup;
  activeDate:any;

  dosageSelectionOptions = [
    {value: "1", viewValue: 'One'},
    {value: "2", viewValue: 'Two'},
    {value: "3", viewValue: 'Three'}
  ];
  constructor(    public authService: AuthService,
                  private builder: FormBuilder,
                  public http: HttpClient,
                  public router: Router,
                  public matDialog : MatDialog)
  { }

  async Submit(formData) {
    let active_date = $('#calendar').evoCalendar('getActiveDate');
    let startDate = active_date;
    let startTime = formData.value["time"];
    let date = new Date(startDate + ' ' + startTime);
    let datePipe = new DatePipe('en-US');
    let newDate = datePipe.transform(date, "yyyy-MM-dd'T'HH:mm:ss");

    console.log("Timestamp generated...");

    let addEventObject = {
      startDateTime: newDate,
      title: formData.value["name"],
      dosage: formData.value["dosage"],
      userId: JSON.parse(localStorage.getItem("user"))["uid"]
    }

    await this.addEvent(addEventObject);

  }

  async addEvent(postData)
  {
    let url = "http://localhost:8080/api/pill-events";
    const httpOptions : Object = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8'
      }),
      responseType: 'application/json'
    };

    const body = JSON.stringify(postData);

    return this.http.post(url, body, httpOptions )
      .toPromise()
      .then(response => {
        console.log(response);
        if(response){
        //  refresh
          this.matDialog.closeAll();
          location.reload();        }
      })
      .catch(console.log);
  }

  ngOnInit() {
    this.myForm = this.builder.group({
      name: new FormControl(),
      time: new FormControl(),
      dosage: new FormControl()
    });
    this.myForm.controls['time'].setValue('12:00');

  }

}
