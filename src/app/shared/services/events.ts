export interface Events {
  name: string;
  date: string;
  type: string;
  id : string;
  dosage: string;
}
