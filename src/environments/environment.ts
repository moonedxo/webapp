// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBVBO73q0Ivsk6Aj1jUQJrrTsZHOfbr1_A',
    authDomain: 'mediteck-865cc.firebaseapp.com',
    databaseURL: 'https://mediteck-865cc.firebaseio.com',
    projectId: 'mediteck-865cc',
    storageBucket: 'mediteck-865cc.appspot.com',
    messagingSenderId: '393498328489'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
