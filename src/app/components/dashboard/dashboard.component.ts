import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Events } from "../../shared/services/events";
import {DatePipe} from "@angular/common";
import {DialogboxComponent} from "../../dialogbox/dialogbox.component";
import {MatDialog} from "@angular/material/dialog";

import { User } from "../../shared/services/user";
// import { ChartType } from 'chart.js';
// import { Chart } from 'chart.js';
// import { MultiDataSet, Label } from 'ng2-charts';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  events : Events;
  myEvents = [
  ];

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone,
    public http: HttpClient,
    public dialog: MatDialog
  ) {

  }

   async getEvents(url, userId){
    console.log("Making request: ");
      return this.http.get(url, {
        params: {
          id: userId
        },
        observe: 'response'
      })
        .toPromise()
        .then(response => {
          console.log(response);
          if(response.body){
            let events = response.body;
            let datePipe = new DatePipe('en-US');

            for(let x in events){
              // console.log("Events[x]",events[x]);
              let date =  datePipe.transform(events[x]["startDateTime"], 'MM/dd/yyyy');
              // console.log("Date: ", date);
              const event: Events = {
                name: events[x]["title"],
                date: date,
                type: "holiday",
                id: events[x]["eventId"],
                dosage: events[x]["dosage"]
              }
              this.myEvents.push(event);
            }
          }
        })
        .catch(console.log);
  }


  handleClick()
  {
    this.dialog.open(DialogboxComponent, {
      height: '400px',
      width: '600px',
    });
  }

  async ngOnInit()
  {
    if(this.authService.isLoggedIn){
      console.log("Can Make Call", JSON.parse(localStorage.getItem("user"))["uid"]);
      let userId = JSON.parse(localStorage.getItem("user"))["uid"];
      let url = "http://localhost:8080/api/pill-events";
      await this.getEvents(url, userId).then(() =>
      {
        if(this.myEvents != []){
          $('#calendar').evoCalendar({
            calendarEvents: this.myEvents,
            format:'mm/dd/yyyy',
            titleFormat:'MM yyyy',
            eventHeaderFormat:'MM d, yyyy',
            todayHighlight:true,
            language:'en',
            theme: "default",
          });
          $(function() {
          //   // var self = this;
          //   $('#calendar').evoCalendar('addCalendarEvent', this.myEvents);
            $("#calendar").evoCalendar('toggleSidebar', false);
            $("#calendar").evoCalendar('toggleEventList', true);
          });

          $('#calendar').on('selectEvent', function(event, activeEvent) {
            // code here...
            console.log("Selected Event ", activeEvent);
          });
        }
      });

    }
    else{
      console.log("User not logged in");
      this.router.navigate(['sign-in']);
    }

  }

}
