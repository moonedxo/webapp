import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PillslidersComponent } from './pillsliders.component';

describe('PillslidersComponent', () => {
  let component: PillslidersComponent;
  let fixture: ComponentFixture<PillslidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PillslidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PillslidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
